using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveLoadManager : ISerializer
{
    string _saveLoadPath;

    public SaveLoadManager(string path)
    {
        _saveLoadPath = path;
    }

    public PlayerData LoadData()
    {
        var fileExists = File.Exists(_saveLoadPath);

        if (fileExists)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(_saveLoadPath, FileMode.Open, FileAccess.Read);
            PlayerData data = (PlayerData)bf.Deserialize(file);
            file.Close();

            return data;
        }
        else Debug.Assert(fileExists, "File with player data has not been created");

        return null;
    }

    public void SaveData(PlayerData playerData)
    {
        var fileExists = File.Exists(_saveLoadPath);

        if (fileExists)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(_saveLoadPath, FileMode.Open, FileAccess.Write);

            bf.Serialize(file, playerData);
            file.Close();
        }
        else Debug.Assert(fileExists, "File with player data has not been created");
    }
}
