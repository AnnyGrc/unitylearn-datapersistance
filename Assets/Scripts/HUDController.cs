using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    [SerializeField] InputField playerNameInput;
    [SerializeField] Button startButton;
    [SerializeField] Button quitButton;
    [SerializeField] TextMeshProUGUI maxScoreText;
    
    PlayerData playerData;
    SaveLoadManager _saveLoadManager;

    string saveDataFilePath;

    void Awake()
    {
        saveDataFilePath = Application.persistentDataPath + "/PlayerSaveData.dat";

        playerData = new PlayerData();
        _saveLoadManager = new SaveLoadManager(saveDataFilePath);

        var fileExists = File.Exists(saveDataFilePath);

        if (!fileExists)
        {
            FileStream file = File.Create(saveDataFilePath);
            file.Close();

            fileExists = File.Exists(saveDataFilePath);

            if (fileExists)
                _saveLoadManager.SaveData(playerData);
            else Debug.Assert(fileExists, "File with player data has not been created");
        }

        playerData = _saveLoadManager.LoadData();
    }

    void Start()
    {
        maxScoreText.text = playerData.playerMaxName + " : " + playerData.playerMaxScore;

        startButton.onClick.AddListener(OnStartButtonClick);
    }

    void OnStartButtonClick()
    {
        playerData.playerCurName = playerNameInput.text;

        _saveLoadManager.SaveData(playerData);

        SceneManager.LoadScene("main", LoadSceneMode.Single);
    }
}
