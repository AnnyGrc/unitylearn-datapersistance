using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISerializer
{
    public PlayerData LoadData();
    public void SaveData(PlayerData playerData);
}
