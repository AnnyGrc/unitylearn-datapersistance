using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class PlayerData
{
    public string playerMaxName;
    public int playerMaxScore;

    public string playerCurName;
    public int playerCurScore;

    public PlayerData()
    {
        playerMaxName = "Player with max score is not set";
        playerMaxScore = 0;

        playerCurName = "Current player name is not set";
        playerCurScore = 0;
    }
}
